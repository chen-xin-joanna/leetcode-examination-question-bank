class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        l = 0
        u = x
        while l<=u:
            mid = (l+u)//2
            if mid*mid <=x:
                ans = mid
                l = mid+1
            else:
                u = mid-1
        return ans

x = 14
c = Solution()
print(c.mySqrt(x))