class Solution(object):
    @classmethod
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        length = len(nums)
        k %= length
        nums[:] = nums[::-1]
        nums[:k] = nums[:k][::-1]
        nums[k:] = nums[k:][::-1]
        return(nums)


nums =  [1,2,3,4,5,6,7]
k = 3
c = Solution()
print(c.rotate(nums, k))