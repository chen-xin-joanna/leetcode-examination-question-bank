class Solution(object):
    def findLengthOfLCIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        ans = 0
        n = len(nums)
        lb = 0

        for i in range(n):
            if i > 0 and nums[i] <= nums[i - 1]:
                lb = i
            ans = max(ans, i - lb + 1)

        return ans

nums =  [1,2,3,4,5,6,7]
c = Solution()
print(c.findLengthOfLCIS(nums))