class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        ans = 0
        n = len(nums)
        count = 0

        for i in range(n):
            if nums[i] ==1:
                count += 1
            else:
                ans = max(ans, count)
                count = 0
        ans = max(ans, count)

        return ans

nums =  [1,1,0,1,1,1,1]
c = Solution()
print(c.findMaxConsecutiveOnes(nums))