class Solution(object):
    def isMonotonic(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        N = len(nums)
        inc = True
        dec = True
        for i in range(1, N):
            if nums[i] < nums[i - 1]:
                inc = False
            if nums[i] > nums[i - 1]:
                dec = False
            if not inc and not dec:
                return False
        return True

nums =  [1,2,3,6,5,6,7]
c = Solution()
print(c.isMonotonic(nums))