class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        return str(bin(int(a, 2) + int(b, 2)))[2:]

a = "1010"
b = "1011"
c = Solution()
print(c.addBinary(a,b))