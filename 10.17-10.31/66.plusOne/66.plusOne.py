class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        i = len(digits) - 1

        while i + 1:
            if digits[i] == 9:
                digits[i] = 0
                i -= 1
            else:
                digits[i] += 1
                return digits
        if digits[0] == 0:
            return [1] + digits

digits = [4,3,2,1]
c = Solution()
print(c.plusOne(digits))