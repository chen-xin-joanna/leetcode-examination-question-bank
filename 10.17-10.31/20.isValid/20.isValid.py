class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if '()' in s or '[]' in s or '{}' in s:
            while '()' in s or '[]' in s or '{}' in s :
                if '()' in s:
                    s = s.replace('()','')
                if '[]' in s:
                    s = s.replace('[]','')
                if '{}' in s:
                    s = s.replace('{}','')
            if len(s) == 0:
                return True
            else:
                return False
        else:
            return False

x="()[]{}"
c = Solution()
print(c.isValid(x))