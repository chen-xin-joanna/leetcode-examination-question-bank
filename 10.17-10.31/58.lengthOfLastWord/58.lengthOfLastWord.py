class Solution(object):
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        ans = 0
        for i in range(n-1, -1, -1):
            if s[i] != " ":
                ans += 1
            else:
                if ans:
                    return ans
        return ans

s = "luffy is still joyboy"
c = Solution()
print(c.lengthOfLastWord(s))
#    def lengthOfLastWord(self, s: str) -> int:
#        return len(s.split( )[-1])

