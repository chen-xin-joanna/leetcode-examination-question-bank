class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        for index, value in enumerate(nums):
            if value >= target:
                return index
        else:
            return len(nums)

nums = [1,3,5,6]
target = 5
c = Solution()
print(c.searchInsert(nums,target))
