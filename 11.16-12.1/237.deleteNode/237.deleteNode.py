# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def getData(self):
        return self.val
    def setNext(self,newnext):
        self.next = newnext

class MyLinkedList:
    def __init__(self):
        self.head = None
    def add(self,val:int) ->None:
        temp = ListNode(val)
        temp.setNext(self.head)
        self.head = temp


class Solution(object):
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        node.val = node.next.val
        node.next = node.next.next

mylist = MyLinkedList()
mylist.add(1)
mylist.add(2)
mylist.add(3)
mylist.add(7)
mylist.add(4)
mylist.add(8)
mylist.add(9)

ans = Solution()
denode = mylist.head.next.next.next
res = ans.deleteNode(denode)
while mylist.head:
    print(mylist.head.getData())
    mylist.head = mylist.head.next