# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def getData(self):
        return self.val
    def setNext(self,newnext):
        self.next = newnext

class MyLinkedList:
    def __init__(self):
        self.head = None
    def add(self,val:int) ->None:
        temp = ListNode(val)
        temp.setNext(self.head)
        self.head = temp


class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        newhead = None
        while head:
            t = head
            head = head.next
            t.next = newhead
            newhead = t
        return newhead


mylist = MyLinkedList()
mylist.add(1)
mylist.add(2)
mylist.add(3)
mylist.add(7)
mylist.add(4)
mylist.add(8)
mylist.add(9)
ans = Solution()
res = ans.reverseList(mylist.head)
print(res.getData())