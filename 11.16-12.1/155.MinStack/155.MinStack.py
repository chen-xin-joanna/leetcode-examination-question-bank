class MinStack(object):

    def __init__(self):
        self.item = []

    def push(self, val):
        """
        :type val: int
        :rtype: None
        """
        self.item.append(val)

    def pop(self):
        """
        :rtype: None
        """
        self.item.pop()

    def top(self):
        """
        :rtype: int
        """
        return self.item[-1]

    def getMin(self):
        """
        :rtype: int
        """
        return min(self.item)

s = MinStack()
s.push(-2)
s.push(0)
s.push(-3)
s.getMin()
s.pop()
s.top()
s.getMin()
print(s.getMin())