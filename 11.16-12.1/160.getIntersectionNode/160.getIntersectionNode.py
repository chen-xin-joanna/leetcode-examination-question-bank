# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def getData(self):
        return self.val
    def setNext(self,newnext):
        self.next = newnext

class MyLinkedList:
    def __init__(self):
        self.head = None
    def add(self,val:int) ->None:
        temp = ListNode(val)
        temp.setNext(self.head)
        self.head = temp


class Solution(object):
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        cura, curb = headA, headB
        while cura != curb:
            cura = cura.next if cura else headB
            curb = curb.next if curb else headA
        return cura



mylist1 = MyLinkedList()
mylist1.add(1)
mylist1.add(2)
mylist1.add(3)
mylist1.add(7)
mylist1.add(4)
mylist1.add(8)
mylist1.add(9)

mylist2 = MyLinkedList()
mylist2.add(2)
mylist2.add(5)
mylist2.add(1)
mylist2.head.next.next.next = mylist1.head.next.next.next.next

ans = Solution()
res = ans.getIntersectionNode(mylist1.head,mylist2.head)
print(res.getData())