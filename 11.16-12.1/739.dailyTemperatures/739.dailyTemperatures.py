class Solution(object):
    def dailyTemperatures(self, temperatures):
        """
        :type temperatures: List[int]
        :rtype: List[int]
        """
        ans = [0] * len(temperatures)
        stack = []
        for i in range(len(temperatures)):
            while stack and temperatures[i] > temperatures[stack[-1]]:
                popindex = stack.pop()
                ans[popindex] = i - popindex
            stack.append(i)
        return ans

c = Solution()
temperatures = [73,74,75,71,69,72,76,73]
ans = c.dailyTemperatures(temperatures)
print(ans)